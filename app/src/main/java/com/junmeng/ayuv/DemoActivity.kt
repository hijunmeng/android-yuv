package com.junmeng.ayuv

import android.graphics.Bitmap
import android.os.Bundle
import com.junmeng.ayuv.databinding.ActivityDemoBinding
import com.junmeng.libyuv.jni.ConvertUtil
import com.junmeng.libyuv.jni.ScaleUtil
import com.junmeng.libyuv.utils.YUVUtil
import java.util.*

class DemoActivity : BaseActivity() {
    lateinit var datas: ByteArray
    var width: Int = 0
    var height: Int = 0
    private lateinit var mBinding: ActivityDemoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityDemoBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        val i420Data: I420Data = MyApp.INSTANCE.i420Data!!
        datas = i420Data.datas
        width = i420Data.width
        height = i420Data.height

        mBinding.info.text = ("size=${datas.size},width=$width,height=$height")

        val byteArray = ConvertUtil.i420ToRGB24(datas, width, height)
        val colors = YUVUtil.rgb24ToColors(byteArray!!, width, height)
        val bmp = Bitmap.createBitmap(
            colors!!, 0, width, width, height,
            Bitmap.Config.RGB_565
        )
        mBinding.sImage.setImageBitmap(bmp)

        mBinding.btnArgb.setOnClickListener {
            handleArgb()
        }
        mBinding.btnY.setOnClickListener {
            handleY()
        }
        mBinding.btnI420scale.setOnClickListener {
            handleI420Scale()
        }

    }

    private fun handleI420Scale() {
        val scale: Float
        if (mBinding.btnI420scale.isSelected) {
            scale = 1.5f
        } else {
            scale = 0.5f
        }
        mBinding.btnI420scale.isSelected = !mBinding.btnI420scale.isSelected
        val byteArray = ScaleUtil.i420Scale(datas, width, height, scale)
        val dstWidth = (width * scale).toInt()
        val dstHeight = (height * scale).toInt()
        val rgb24 = ConvertUtil.i420ToRGB24(byteArray!!, dstWidth, dstHeight)
        val colors = YUVUtil.rgb24ToColors(rgb24!!, dstWidth, dstHeight)
        val bmp = Bitmap.createBitmap(
            colors!!, 0, dstWidth, dstWidth, dstHeight,
            Bitmap.Config.RGB_565
        )
        mBinding.ivArgb.setImageBitmap(bmp)
    }


    private fun handleArgb() {
        var byteArray = ConvertUtil.i420ToARGB(datas, width, height)
        val scale: Float
        if (mBinding.ivArgb.isSelected) {
            scale = 1.5f
        } else {
            scale = 0.5f
        }
        mBinding.ivArgb.isSelected = !mBinding.ivArgb.isSelected
        val dstWidth = (width * scale).toInt()
        val dstHeight = (height * scale).toInt()
        byteArray = ScaleUtil.argbScale(byteArray!!, width, height, scale)
        val colors = YUVUtil.argbToColors(byteArray!!, dstWidth, dstHeight)
        val bmp = Bitmap.createBitmap(
            colors!!, 0, dstWidth, dstWidth, dstHeight,
            Bitmap.Config.ARGB_8888
        )
        mBinding.ivArgb.setImageBitmap(bmp)
    }

    private fun handleY() {
        val colors = YUVUtil.yPlaneToColors(datas.take(width * height).toByteArray(), width, height)
        val bmp = Bitmap.createBitmap(
            colors!!, 0, width, width, height,
            Bitmap.Config.RGB_565
        )
        mBinding.ivArgb.setImageBitmap(bmp)
    }
}