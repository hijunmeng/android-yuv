package com.junmeng.ayuv

import android.app.Application
import androidx.camera.camera2.Camera2Config
import androidx.camera.core.CameraXConfig

class MyApp : Application(), CameraXConfig.Provider {
    var i420Data: I420Data? = null

    companion object {
        lateinit var INSTANCE: MyApp
    }

    override fun onCreate() {
        INSTANCE = this
        super.onCreate()
    }

    override fun getCameraXConfig(): CameraXConfig {
        return CameraXConfig.Builder.fromConfig(Camera2Config.defaultConfig())
            .build()
    }
}