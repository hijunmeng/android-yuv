package com.junmeng.ayuv

import java.io.Serializable

data class I420Data(
    val datas:ByteArray,
    val width:Int,
    val height:Int
):Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as I420Data

        if (!datas.contentEquals(other.datas)) return false
        if (width != other.width) return false
        if (height != other.height) return false

        return true
    }

    override fun hashCode(): Int {
        var result = datas.contentHashCode()
        result = 31 * result + width
        result = 31 * result + height
        return result
    }
}
