package com.junmeng.ayuv

import androidx.camera.core.ImageProxy
import com.junmeng.ayuv.utils.ImageProxyUtil

/**
 * 获得格式为ImageFormat.YUV_420_888的y分量的数据
 * @receiver ImageProxy
 * @param outBytes ByteArray 为null时，则结果从返回值获取；不为null时，表明用户希望将结果放入此变量，此设计的目的是可以将缓存交给外部管理，可避免频繁创建和释放的问题，因此推荐用户使用此方式
 * @return ByteArray
 */
@Throws(RuntimeException::class)
fun ImageProxy.getYPlaneDatas(outBytes: ByteArray? = null): ByteArray {
    return ImageProxyUtil.getYPlane(this, outBytes)
}

@Throws(RuntimeException::class)
fun ImageProxy.getI420Datas(outBytes: ByteArray? = null): ByteArray {
    return ImageProxyUtil.getI420Datas(this, outBytes)
}

@Throws(RuntimeException::class)
fun ImageProxy.getNV12Datas(outBytes: ByteArray? = null): ByteArray {
    return ImageProxyUtil.getNV12Datas(this, outBytes)
}

@Throws(RuntimeException::class)
fun ImageProxy.getNV21Datas(outBytes: ByteArray? = null): ByteArray {
    return ImageProxyUtil.getNV21Datas(this, outBytes)
}
