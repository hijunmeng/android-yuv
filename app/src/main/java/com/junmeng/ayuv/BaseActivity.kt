package com.junmeng.ayuv

import android.content.pm.PackageManager
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat

open class BaseActivity : AppCompatActivity() {
    open fun checkAndRequestPermission(
        permission: String,
        grantCallback: Runnable?,
        denyCallback: Runnable? = null
    ) {
        if (ActivityCompat.checkSelfPermission(
                this,
                permission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            grantCallback?.run()
            return
        }
        val resultLauncher =
            registerForActivityResult(ActivityResultContracts.RequestPermission()) {
                if (it) {
                    grantCallback?.run()
                } else {
                    denyCallback?.run()
                }
            }
        //有的设备两次拒绝后就不再弹窗了
        resultLauncher.launch(permission)
    }

    open fun showToast(text: String) {
        runOnUiThread {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
        }
    }
}