#include <jni.h>
#include <string>
#include "libyuv/convert_argb.h"
#include "ALog.h"


extern "C" jint
Java_com_junmeng_libyuv_jni_ConvertUtil_i420ToARGB(JNIEnv *env, jobject thiz, jbyteArray src,
                                                   jint width, jint height, jbyteArray dst) {
    uint8_t *_src = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(src, nullptr));
    uint8_t *_dst = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(dst, nullptr));
    int ret = libyuv::I420ToARGB(
            _src,
            width,
            _src + width * height,
            width / 2,
            _src + width * height * 5 / 4,
            width / 2,
            _dst,
            width * 4,//dst_stride_argb
            width,
            height);
    if (ret == 0) {
        LOGI("I420ToARGB success");
    } else {
        LOGE("I420ToARGB fialed:%d", ret);
    }
    env->ReleaseByteArrayElements(src, reinterpret_cast<jbyte *>(_src), JNI_ABORT);
    env->ReleaseByteArrayElements(dst, reinterpret_cast<jbyte *>(_dst), JNI_ABORT);
    return ret;
}

extern "C" jint
Java_com_junmeng_libyuv_jni_ConvertUtil_i420ToRGB24(JNIEnv *env, jobject thiz, jbyteArray src,
                                                    jint width,
                                                    jint height, jbyteArray dst) {
    uint8_t *_src = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(src, nullptr));
    uint8_t *_dst = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(dst, nullptr));
    int ret = libyuv::I420ToRGB24(
            _src,
            width,
            _src + width * height,
            width / 2,
            _src + width * height * 5 / 4,
            width / 2,
            _dst,
            width * 3,//dst_stride_argb
            width,
            height);
    if (ret == 0) {
        LOGI("I420ToRGB24 success");
    } else {
        LOGE("I420ToRGB24 fialed:%d", ret);
    }
    env->ReleaseByteArrayElements(src, reinterpret_cast<jbyte *>(_src), JNI_ABORT);
    env->ReleaseByteArrayElements(dst, reinterpret_cast<jbyte *>(_dst), JNI_ABORT);
    return ret;
}

extern "C" jint
Java_com_junmeng_libyuv_jni_ConvertUtil_nv12ToRGB24(JNIEnv *env, jobject thiz, jbyteArray src,
                                                    jint width,
                                                    jint height, jbyteArray dst) {
    uint8_t *_src = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(src, nullptr));
    uint8_t *_dst = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(dst, nullptr));
    int ret = libyuv::NV12ToRGB24(
            _src,
            width,
            _src + width * height,
            width,
            _dst,
            width * 3,//dst_stride_argb
            width,
            height);
    if (ret == 0) {
        LOGI("NV12ToRGB24 success");
    } else {
        LOGE("NV12ToRGB24 fialed:%d", ret);
    }
    env->ReleaseByteArrayElements(src, reinterpret_cast<jbyte *>(_src), JNI_ABORT);
    env->ReleaseByteArrayElements(dst, reinterpret_cast<jbyte *>(_dst), JNI_ABORT);
    return ret;
}

extern "C" jint
Java_com_junmeng_libyuv_jni_ConvertUtil_nv21ToRGB24(JNIEnv *env, jobject thiz, jbyteArray src,
                                                    jint width,
                                                    jint height, jbyteArray dst) {
    uint8_t *_src = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(src, nullptr));
    uint8_t *_dst = reinterpret_cast<uint8_t *>(env->GetByteArrayElements(dst, nullptr));
    int ret = libyuv::NV21ToRGB24(
            _src,
            width,
            _src + width * height,
            width,
            _dst,
            width * 3,//dst_stride_argb
            width,
            height);
    if (ret == 0) {
        LOGI("NV21ToRGB24 success");
    } else {
        LOGE("NV21ToRGB24 fialed:%d", ret);
    }
    env->ReleaseByteArrayElements(src, reinterpret_cast<jbyte *>(_src), JNI_ABORT);
    env->ReleaseByteArrayElements(dst, reinterpret_cast<jbyte *>(_dst), JNI_ABORT);
    return ret;
}