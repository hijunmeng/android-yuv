package com.junmeng.libyuv.jni

object ConvertUtil {
    init {
        System.loadLibrary("yuv")
    }

    /**
     * i420格式转rgb
     */
    external fun i420ToRGB24(
        src: ByteArray,//i420格式字节数组，例如yyyy yyyy uu vv
        width: Int, height: Int,//宽高
        dst: ByteArray//用于存放转换后的rgb数据
    ): Int

    /**
     * nv12格式转rgb(3字节)
     */
    external fun nv12ToRGB24(
        src: ByteArray,//nv12格式字节数组，例如yyyy yyyy uv uv
        width: Int, height: Int,
        dst: ByteArray//用于存放转换后的rgb数据
    ): Int

    /**
     * nv21格式转rgb(3字节)
     */
    external fun nv21ToRGB24(
        src: ByteArray,//nv12格式字节数组，例如yyyy yyyy vu vu
        width: Int, height: Int,
        dst: ByteArray//用于存放转换后的rgb数据
    ): Int

    /**
     * i420格式转argb（4字节）
     */
    external fun i420ToARGB(
        src: ByteArray, //i420格式字节数组，例如yyyy yyyy uu vv
        width: Int, height: Int,
        dst: ByteArray//用于存放转换后的rgb数据
    ): Int


    /**
     * i420格式转argb（4字节）
     */
    fun i420ToARGB(
        src: ByteArray,//i420格式字节数组，例如yyyy yyyy uu vv
        width: Int, height: Int
    ): ByteArray? {
        val dst = ByteArray(width * height * 4)
        if (i420ToARGB(src, width, height, dst) != 0) {
            return null
        }
        return dst
    }

    /**
     * nv12格式转rgb(3字节)
     */
    fun i420ToRGB24(
        src: ByteArray,//i420格式字节数组，例如yyyy yyyy uu vv
        width: Int, height: Int
    ): ByteArray? {
        val dst = ByteArray(width * height * 3)
        if (i420ToRGB24(src, width, height, dst) != 0) {
            return null
        }
        return dst
    }

    /**
     * nv12格式转rgb(3字节)
     */
    fun nv12ToRGB24(
        src: ByteArray, //nv12格式字节数组，例如yyyy yyyy uv uv
        width: Int, height: Int
    ): ByteArray? {
        val dst = ByteArray(width * height * 3)
        if (nv12ToRGB24(src, width, height, dst) != 0) {
            return null
        }
        return dst
    }

    /**
     * nv21格式转rgb(3字节)
     */
    fun nv21ToRGB24(
        src: ByteArray, //nv12格式字节数组，例如yyyy yyyy vu vu
        width: Int, height: Int
    ): ByteArray? {
        val dst = ByteArray(width * height * 3)
        if (nv21ToRGB24(src, width, height, dst) != 0) {
            return null
        }
        return dst
    }
}