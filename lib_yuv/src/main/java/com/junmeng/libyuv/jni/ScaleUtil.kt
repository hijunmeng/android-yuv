package com.junmeng.libyuv.jni

object ScaleUtil {
    init {
        System.loadLibrary("yuv")
    }

    /**
     * 缩放i420
     */
    external fun i420Scale(
        src: ByteArray, //例如 yyyy yyyy uu vv
        srcWidth: Int, srcHeight: Int,
        dst: ByteArray,//用于存放缩放后的数据，例如 yyyy yyyy uu vv
        dstWidth: Int, dstHeight: Int, //目标宽高
        mode: Int = 0 //0--Point sample; Fastest. 1--Filter horizontally only 2--Faster than box, but lower quality scaling down 3--Highest quality
    ): Int

    /**
     * 缩放argb
     */
    external fun argbScale(
        src: ByteArray,
        srcWidth: Int, srcHeight: Int,
        dst: ByteArray,//用于存放缩放后的数据
        dstWidth: Int, dstHeight: Int,//目标宽高
        mode: Int = 0
    ): Int

    /**
     * 缩放i420
     */
    fun i420Scale(
        src: ByteArray,
        srcWidth: Int, srcHeight: Int,
        dstWidth: Int, dstHeight: Int,//目标宽高
        mode: Int = 0
    ): ByteArray? {
        if (srcWidth <= 0 || srcHeight <= 0) {
            return null
        }
        val size = srcWidth * srcHeight * 3 / 2
        if (src.size != size) {
            return null
        }

        if (dstWidth <= 0 || dstHeight <= 0) {
            return null
        }
        val dst = ByteArray(dstWidth * dstHeight * 3 / 2)
        if (i420Scale(src, srcWidth, srcHeight, dst, dstWidth, dstHeight, mode) != 0) {
            return null
        }
        return dst
    }

    /**
     * 缩放i420
     */
    fun i420Scale(
        src: ByteArray,
        srcWidth: Int, srcHeight: Int,
        scale: Float, //缩放比例
        mode: Int = 0
    ): ByteArray? {
        val dstWidth = (srcWidth * scale).toInt()
        val dstHeight = (srcHeight * scale).toInt()
        return i420Scale(src, srcWidth, srcHeight, dstWidth, dstHeight, mode)
    }

    /**
     * 缩放argb
     */
    fun argbScale(
        src: ByteArray,
        srcWidth: Int, srcHeight: Int,
        dstWidth: Int, dstHeight: Int,
        mode: Int = 0 //0-- 1-- 2-- 3--
    ): ByteArray? {
        if (srcWidth <= 0 || srcHeight <= 0) {
            return null
        }
        val size = srcWidth * srcHeight * 4
        if (src.size != size) {
            return null
        }
        if (dstWidth <= 0 || dstHeight <= 0) {
            return null
        }
        val dst = ByteArray(dstWidth * dstHeight * 4)
        if (argbScale(src, srcWidth, srcHeight, dst, dstWidth, dstHeight, mode) != 0) {
            return null
        }
        return dst
    }

    /**
     * 缩放argb
     */
    fun argbScale(
        src: ByteArray,
        srcWidth: Int, srcHeight: Int,
        scale: Float, //缩放比例
        mode: Int = 0
    ): ByteArray? {
        val dstWidth = (srcWidth * scale).toInt()
        val dstHeight = (srcHeight * scale).toInt()
        return argbScale(src, srcWidth, srcHeight, dstWidth, dstHeight, mode)
    }

}