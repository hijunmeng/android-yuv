package com.junmeng.libyuv.jni


object RotationUtil {
    init {
        System.loadLibrary("yuv")
    }

    /**
     * 旋转平面
     */
    external fun rotatePlane(
        src: ByteArray,//待旋转的数据
        srcStride: Int,//行距，一般为width
        dst: ByteArray,//旋转后的数据
        dstStride: Int,//旋转后数据的行距，如果旋转角度为90或270，则一般为height,否则为width
        width: Int,//未旋转前的宽度
        height: Int,//未旋转前的高度
        degree: Int//顺时针旋转角度，只能取0,90,180,270
    ): Int

    /**
     * 旋转i420格式
     */
    external fun i420Rotate(
        src: ByteArray, //例如 yyyy yyyy uu vv
        srcWidth: Int, srcHeight: Int,
        dst: ByteArray,//例如 yyyy yyyy uu vv
        degree: Int//顺时针旋转角度，只能取0,90,180,270
    ): Int

    /**
     * nv12格式旋转角度并转为i420格式存储在dst
     */
    external fun nv12ToI420Rotate(
        src: ByteArray,//例如 yyyy yyyy uv uv
        srcWidth: Int, srcHeight: Int,
        dst: ByteArray,//例如 yyyy yyyy uu vv
        degree: Int//顺时针旋转角度，只能取0,90,180,270
    ): Int

    /**
     * nv12格式旋转角度并转为i420返回
     */
    fun nv12ToI420Rotate(
        src: ByteArray,  //例如 yyyy yyyy uv uv
        srcWidth: Int, srcHeight: Int,
        degree: Int = 90 //顺时针旋转角度，只能取0,90,180,270
    ): ByteArray? {
        if (srcWidth <= 0 || srcHeight <= 0) {
            return null
        }
        val size = srcWidth * srcHeight * 3 / 2
        if (src.size != size) {
            return null
        }
        val dst = ByteArray(size)
        if (nv12ToI420Rotate(src, srcWidth, srcHeight, dst, degree) != 0) {
            return null
        }
        return dst
    }

    /**
     * 旋转i420格式
     */
    fun i420Rotate(
        src: ByteArray, //例如 yyyy yyyy uu vv
        srcWidth: Int, srcHeight: Int,
        degree: Int = 90 //顺时针旋转角度，只能取0,90,180,270
    ): ByteArray? {
        if (srcWidth <= 0 || srcHeight <= 0) {
            return null
        }
        val size = srcWidth * srcHeight * 3 / 2
        if (src.size != size) {
            return null
        }
        val dst = ByteArray(size)
        if (i420Rotate(src, srcWidth, srcHeight, dst, degree) != 0) {
            return null
        }
        return dst
    }

    /**
     * 旋转y平面
     */
    fun rotateYPlane(
        src: ByteArray,//待旋转的y分量的数据，大小等于宽高的乘积
        width: Int, height: Int, //未旋转前的宽高
        degree: Int = 90 //顺时针旋转角度，只能取0,90,180,270
    ): ByteArray? {
        if (width <= 0 || height <= 0) {
            return null
        }
        if (src.size != width * height) {
            return null
        }
        var dstStride = width
        if (degree == 90 || degree == 270) {
            dstStride = height
        }
        val dst = ByteArray(width * height)
        if (rotatePlane(src, width, dst, dstStride, width, height, degree) != 0) {
            return null
        }
        return dst
    }
}